# frozen_string_literal: true

require 'json'
require 'open3'
require 'loofah'
require 'stopwords'

module Jekyll
  module Lunr
    # Error
    class Error < StandardError; end

    # Add the indexer to the site
    module IndexableSite
      def self.included(base)
        base.class_eval do
          def indexer
            @indexer ||= Jekyll::Lunr::Indexer.new self
          end
        end
      end
    end

    # Index a Jekyll::Document
    module IndexableDocument
      def self.included(base)
        base.class_eval do
          def to_data
            data
              .slice(*site.indexer.fields)
              .transform_values { |value| extract_data_recursively(value) }
              .merge('content' => Loofah.fragment(content).to_text.strip,
                     'id' => url,
                     'year' => date&.year)
          end

          def extract_data_recursively(value)
            case value
            when Array then value.map { |v| extract_data(v) }
            when Hash then value.transform_values { |v| extract_data(v) }
            when Jekyll::Document then extract_data(value)
            when Jekyll::Page then extract_data(value)
            when String
              if value.include? '<'
                Loofah.fragment(value).to_text.strip
              else
                value.strip
              end
            else value
            end
          end

          def extract_data(value)
            value.respond_to?(:to_data) ? value.to_data : value
          end
        end
      end
    end

    # Generates a Lunr index from documents
    class Indexer
      attr_reader :site

      def initialize(site)
        @site = site
      end

      # The data is the register where Lunr looks for results.
      # TODO: Write a single data file per doc?
      def data
        @data ||= site.documents.select do |doc|
          doc.respond_to? :to_data
        end.reject do |doc|
          doc.data['sitemap'] == false
        end.map(&:to_data)
      end

      def data_file
        @data_file ||= Jekyll::StaticFile.new(site, site.source, '.', 'data.json')
      end

      # Convert data to strings since Lunr can't index objects
      def indexable_data
        @indexable_data ||= data.map do |d|
          d.transform_values do |v|
            case v
            when Array
              v.map do |vv|
                vv.is_a?(Hash) ? vv['title'] : vv
              end.compact.join(', ')
            when Hash then v['title'] || v.values.map(&:to_s).join(', ')
            else v.to_s
            end
          end
        end
      end

      def cleanup(data)
        cleaned_data = data.dup

        if (lang = lang&.to_sym)
          sieve = Stopwords::Snowball::WordSieve.new
          words = cleaned_data.split(' ').map(&:strip)

          cleaned_data = site.filter(lang: lang, words: words).join(' ')
        end

        cleaned_data
      end

      def write
        File.open(data_file.path, 'w') do |df|
          df.write data.to_json
        end

        site.static_files << data_file
      end

      def dir
        File.realpath(File.join([__dir__, '..', '..']))
      end

      def index_file
        @index_file ||= Jekyll::StaticFile.new(site, site.source, '.', 'idx.json')
      end

      def indexer
        @indexer ||= ['node', File.join(dir, 'lib', 'assets', 'javascript', 'indexer.js'), lang].freeze
      end

      def env
        @env ||= { 'NODE_PATH' => File.join(site.source, 'node_modules') }
      end

      def index
        Open3.popen2(env, *indexer) do |stdin, stdout, wait|
          indexable_data.each do |data|
            stdin.puts(data.transform_values do |val|
              case val
              when String then cleanup val
              else val
              end
            end.to_json)
          end
          stdin.close

          File.open(index_file.path, 'w') do |idx|
            idx.write(stdout.read)
          end

          site.static_files << index_file

          wait.value
        end
      end

      # Site lang
      def lang
        @lang ||= site.config['lang'].freeze
      end

      # Indexable fields
      def fields
        @fields ||= Set.new((site.config.dig('jekyll-lunr', 'fields') || []) + %w[title description]).freeze
      end

      def free
        @data = nil
        @indexable_data = nil
      end
    end
  end
end

Jekyll::Site.include Jekyll::Lunr::IndexableSite
Jekyll::Document.include Jekyll::Lunr::IndexableDocument
Jekyll::Page.include Jekyll::Lunr::IndexableDocument
